import React, { Component } from 'react';

import Card from 'components/Card/Card.jsx';
import UserTable from 'main/user/UserTable';


class TableList extends Component {

    render() {
        return (
            <div className="content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <Card
                                title="Users"
                                category="Here is the information on all the available Users"
                                contentClass="table-responsive table-full-width"
                                content={
                                    <UserTable/>
                                }
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TableList;
