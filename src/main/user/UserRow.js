import React from 'react';
import axios from 'axios';


const appBaseUrl="/api";

class User extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            id:"",
            name:"",
            age:"",
            salary:"",
            isEditable:false,
            isNewEntry:false,
            isDeleted:false,
            _notificationSystem: null
        };
        this.handleChange=this.handleChange.bind(this);
        this.handleCreate=this.handleCreate.bind(this);
        this.handleEdit=this.handleEdit.bind(this);
        this.handleUpdate=this.handleUpdate.bind(this);
        this.handleCancelEdit=this.handleCancelEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleUpdate(event){
        axios.put(appBaseUrl+'/user/'+this.state.id,{
            name:this.state.tempName,
            age:this.state.tempAge,
            salary:this.state.tempSalary
        }).then(res => {
            console.log(res);
            this.setState({
                id:res.data.id,
                name:res.data.name,
                age:res.data.age,
                salary:res.data.salary,
                isEditable:false
            });
        }).catch(error => {
            console.log(error)
        });
    }

    handleCancelEdit(){
        this.setState({
            isEditable:false
        });
    }

    handleEdit(){
        this.setState({
            isEditable:true,
            tempName:this.state.name,
            tempAge:this.state.age,
            tempSalary:this.state.salary
        });
    }

    handleChange(event){
        const target=event.target;
        this.setState({
            [target.name]:target.value
        })
    }


    handleDelete(){
        axios.delete(appBaseUrl + '/user/' + this.state.id)
            .then(response => {
                console.log(response);
                this.setState({
                    isDeleted:true
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    handleCreate(){
        axios.post(appBaseUrl+'/user/',{
            name:this.state.name,
            age:this.state.age,
            salary:this.state.salary
        }).then(res => {
            console.log(res);
            this.setState({
                id:res.data.id,
                isNewEntry:false
            });
        }).catch(error => {
            console.log(error)
        });
        this.props.postCreate();

    }


    componentDidMount() {
        this.setState({
            id: this.props.user.id,
            name: this.props.user.name,
            age: this.props.user.age,
            salary: this.props.user.salary,
            isEditable: this.props.isEditable,
            isNewEntry:this.props.isNewEntry,
            _notificationSystem: this.refs.notificationSystem
        });
    }


    render() {
        let row = null;
        if (this.state.isEditable !== true && this.state.isNewEntry !== true && this.state.isDeleted !== true){
            row=<tr   key={this.state.id}>
                <td>{this.state.name}</td>
                <td>{this.state.age}</td>
                <td>{this.state.salary}</td>
                <td>
                    <button className="btn btn-info" onClick={this.handleEdit}>
                        Edit
                    </button> &nbsp;
                    <button className="btn btn-info" onClick={this.handleDelete}>
                        Delete
                    </button>
                </td>
            </tr>;
        }
        else if (this.state.isEditable === true){
            row=<tr    key={this.state.id}>
                <td><input name="tempName" value={this.state.tempName} onChange={this.handleChange} type="text"/></td>
                <td><input name="tempAge" value={this.state.tempAge} onChange={this.handleChange} type="text"/></td>
                <td><input name="tempSalary" value={this.state.tempSalary} onChange={this.handleChange} type="text"/></td>
                <td>
                    <button className="btn btn-info" onClick={this.handleUpdate}>
                        Update
                    </button> &nbsp;
                    <button className="btn btn-info" onClick={this.handleCancelEdit}>
                        Cancel
                    </button>
                </td>
            </tr>;
        }
        else if (this.state.isNewEntry === true){
            row=<tr   key={this.state.id}>
                <td><input name="name" value={this.state.name} onChange={this.handleChange} type="text"  /></td>
                <td><input name="age" value={this.state.age} onChange={this.handleChange} type="text" /></td>
                <td><input name="salary" value={this.state.salary} onChange={this.handleChange} type="text" /></td>
                <td>
                    <button className="btn btn-info" onClick={this.handleCreate} >
                        Add
                    </button>

                </td>
            </tr>;
        }
        console.log(row);
        return (row);
    }
}

export default User;