import React from 'react';
import axios from 'axios';
import User from 'main/user/UserRow';
import {Table} from 'react-bootstrap';

const appBaseUrl="/api";


class UserTable extends React.Component{
    //var userCount;
    constructor(props){
        super(props);
        this.state = {
            userRows: []
        };
        this.userCount=0;
        this.handlePostCreate = this.handlePostCreate.bind(this);
    }

    newUserRow(){
        this.userCount+=1;
        return(
            <User
                key={this.userCount}
                user={
                    {id:"new",
                        name:"",
                        age:"",
                        salary:""}
                }
                isEditable={false}
                isNewEntry={true}
                postCreate={this.handlePostCreate}
                entryType="new"/>
        );
    }

    loadUsersFromServer() {
        axios.get(appBaseUrl+'/user/')
            .then(res => {
                console.log(res);
                const users = res.data;
                const rows = [];
                if (users) {
                    users.forEach(user => {
                        console.log(user);
                        this.userCount += 1;
                        rows.push(
                            <User
                                key={this.userCount}
                                user={user}
                                onDelete={this.handleDelete}
                                postCreate={this.handlePostCreate}
                                isEditable={false}
                                isNewEntry={false}
                            />);
                    });
                }
                rows.push(this.newUserRow());
                console.log(rows);
                this.setState({
                    userRows:rows
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    handlePostCreate(){
        const rows = this.state.userRows;
        rows.push(this.newUserRow());
        this.setState({
            userRows:rows
        });
    }


    componentDidMount() {
        this.loadUsersFromServer();
    }

    render(){
        return (
            <Table striped hover>
                <thead>
                <tr>
                    <th >Name</th>
                    <th >Age</th>
                    <th >Salary</th>
                    <th >Options</th>
                </tr>
                </thead>
                <tbody>
                {this.state.userRows}
                </tbody>
            </Table>
        );
    }
}

export default UserTable